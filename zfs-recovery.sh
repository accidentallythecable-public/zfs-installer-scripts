#!/usr/bin/env bash


source /etc/os-release


print_help() {
    echo -e "ZFS RECOVERY"
    echo -e "Options:"
    echo -e "\t-h\t\tHelp Menu"
    echo -e "\t-R <CODENAME>\t\tDebian Codename that is on OS disks, Default: ${VERSION_CODENAME}"
    echo -e "\t-b\t\tEnable Backports"
    exit 1
}


echo "Starting.."

if [ "$EUID" -ne 0 ]; then
    echo "Must be run as root" 1>&2
    exit 1
fi

ENABLE_BACKPORTS=0
while getopts 'bhR:' opt; do
        case $opt in
                b) ENABLE_BACKPORTS=1
                        ;;
                R) VERSION_CODENAME=$OPTARG
                        ;;
                *) print_help
                        ;;
        esac
done

export DEBIAN_FRONTEND=noninteractive

cat <<EOF > /etc/apt/sources.list
deb http://deb.debian.org/debian ${VERSION_CODENAME} main contrib
deb-src http://deb.debian.org/debian ${VERSION_CODENAME} main contrib

deb http://deb.debian.org/debian-security ${VERSION_CODENAME}-security main contrib
deb-src http://deb.debian.org/debian-security ${VERSION_CODENAME}-security main contrib

deb http://deb.debian.org/debian ${VERSION_CODENAME}-updates main contrib
deb-src http://deb.debian.org/debian ${VERSION_CODENAME}-updates main contrib
EOF

if [ ${ENABLE_BACKPORTS} -eq 1 ]; then
    BACKPORTS_FLAG="-t ${VERSION_CODENAME}-backports"
    echo "deb http://deb.debian.org/debian ${VERSION_CODENAME}-backports main contrib non-free" >> /etc/apt/sources.list
fi

apt update
apt -y install mokutil

SECURE_BOOT=0
if [ "$(mokutil --sb-state)" == "SecureBoot enabled" ]; then
    echo "SECURE BOOT IS NOT SUPPORTED, PLEASE DISABLE SECURE BOOT, AND RUN THIS AGAIN"
    exit 3
    # SECURE_BOOT=1
    # echo "Secure Boot is enabled. Ensuring we have things for signing"
    # apt -y install --no-install-recommends --no-install-suggests shim-signed shim-helpers-amd64-signed
    # echo -e "notpassword\nnotpassword" | mokutil --import /var/lib/dkms/mok.pub
fi

apt -y install gdisk dkms linux-headers-$(uname -r) ${BACKPORTS_FLAG}
apt -y install --no-install-recommends zfs-dkms ${BACKPORTS_FLAG}

modprobe zfs

apt -y install zfsutils-linux ${BACKPORTS_FLAG}

#### Prep Complete

echo "Prep complete, you can do everything else by hand and Ctrl+C this script, or wait 5 seconds for this script to do the needful"
sleep 5

echo "Importing Pools"
zpool import rpool -d /dev/disk/by-id/ -R /mnt
zpool import bpool -d /dev/disk/by-id/ -R /mnt

echo "Mounting PROC SYS DEV"
for p in proc sys dev; do
    mount --rbind /${p} /mnt/${p}
done

echo "Mounting all fstab entries"
chroot /mnt /usr/bin/mount -a

echo "Pools imported temporarily at /mnt"
echo "Mounts are listed below"
echo "----"
mount -l | grep zfs | awk '{print $1 " " $2 " " $3};'

