#!/usr/bin/env bash
# Copyright 2024-2024 by AccidentallyTheCable <cableninja@cableninja.net>.
# All rights reserved.
# This file is part of AccidentallyTheCables ZFS Installer Scripts,
# and is released under "AGPLv3". Please see the LICENSE
# file that should have been included as part of this package.
#### END COPYRIGHT BLOCK ###

#set -e
#set -o pipefail

source /etc/os-release

ROOT_SIZE=64G

TOUCHED_DISKS=""

touched_disk() {
    local d
    local serial
    d=$1
    serial=$(lsblk -S $d | grep disk | awk '{print $7}')
    if [ "$(echo "${TOUCHED_DISKS}" | grep "${serial}")" != "" ]; then
        #echo "ALREADY TOUCHED ${d} Serial: ${serial}"
        echo 1
    else
        #echo "HAVE NOT TOUCHED ${d} YET Serial: ${serial}"
        TOUCHED_DISKS="${TOUCHED_DISKS} ${serial}"
        echo 0
    fi
}

print_help() {
    echo -e "ZFS INSTALLER"
    echo -e "Options:"
    echo -e "\t-A\t\tSearch for disks by id (scsi- or wwn-), and use all discovered disks as a mirror"
    echo -e "\t-h\t\tHelp Menu"
    echo -e "\t-R <CODENAME>\t\tDebian Codename to install, Default: ${VERSION_CODENAME}"
    echo -e "\t-H <HOSTNAME>\t\tSet Hostname, Default: <hostid>"
    echo -e "\t-d <DISK_ID>\t\tDisk to use"
    echo -e "\t-Z <SIZE>\t\trpool Disk Size, should be in format of <size>[MGT], Default: 64G"
    echo -e "\nPassing Specific Disks:"
    echo -e "\t\tOnly Pass 'wwn-<id>' or 'scsi-<id>'. Must be a /dev/disk/by-id/ path"
    echo ""
    exit 2
}

echo "Starting.."

if [ "$EUID" -ne 0 ]; then
    echo "Must be run as root" 1>&2
    exit 1
fi

if [ "$1" == "" ]; then
    print_help
fi

ALL_DISKS=0

while getopts 'H:hd:R:AZ:' opt; do
        case $opt in
                A) ALL_DISKS=1
                        ;;
                H) HOSTNAME=$OPTARG
                        ;;
                R) VERSION_CODENAME=$OPTARG
                        ;;
                d) DISKS+=("$OPTARG")
                        ;;
                Z) ROOT_SIZE=$OPTARG
                        ;;
                *) print_help
                        ;;
        esac
done

if [ "$(echo $ROOT_SIZE | egrep "[0-9]{1,}[GTM]")" == "" ]; then
    echo "Cannot Parse Disk Size, should be in format of <size>[MGT], EX: 64G" 1>&2
    exit 1
fi

if [ "${HOSTNAME}" == "" ]; then
    echo "Hostname was not set, using hostid"
    HOSTNAME=$(hostid)
fi

if [ $ALL_DISKS -eq 1 ]; then
    echo "Searching for Disks"
    DISKS=($(ls -alh /dev/disk/by-id/ | egrep "wwn-[a-fA-Fx0-9]{1,} -" | awk '{print $9;}'))
    if [ -z $DISKS ]; then
        echo "Could not find any disks via wwn-, attempting scsi- location" 1>&2
        DISKS=($(ls -alh /dev/disk/by-id/ | egrep "scsi-[a-fA-Fx0-9]{1,} -" | awk '{print $9;}'))
    fi
    if [ -z $DISKS ]; then
        echo "Could not find any disks by id via scsi- or wwn- ids" 1>&2
        exit 1
    fi
else
    echo "Using Disks specified from commandline"
    echo "Disks: ${DISKS[*]}"
fi

if [ ${#DISKS[@]} -eq 0 ]; then
    echo "No Disks Found or Specified" 1>&2
    exit 1
fi

export DEBIAN_FRONTEND=noninteractive

echo "deb http://deb.debian.org/debian/ ${VERSION_CODENAME} main contrib" > /etc/apt/sources.list

apt update
apt -y install mokutil

SECURE_BOOT=0
if [ "$(mokutil --sb-state)" == "SecureBoot enabled" ]; then
    echo "SECURE BOOT IS NOT SUPPORTED, PLEASE DISABLE SECURE BOOT, AND RUN THIS AGAIN"
    exit 3
    # SECURE_BOOT=1
    # echo "Secure Boot is enabled. Ensuring we have things for signing"
    # apt -y install --no-install-recommends --no-install-suggests shim-signed shim-helpers-amd64-signed
    # echo -e "notpassword\nnotpassword" | mokutil --import /var/lib/dkms/mok.pub
fi

apt -y install debootstrap gdisk dkms dpkg-dev linux-headers-$(uname -r)
apt -y install --no-install-recommends zfs-dkms

modprobe zfs

# if [ "$(lsmod | grep zfs)" == "" ]; then
#     ZFS_VERSION=$(dpkg -l | grep zfs-dkms | awk '{print $3;}' | sed -r 's/\-[0-9]{1,}$//g')
#     if [ $SECURE_BOOT -eq 1 ]; then
#         dkms build -f zfs/$ZFS_VERSION
#         dkms install zfs/$ZFS_VERSION
#         modprobe zfs
#         if [ "$(lsmod | grep zfs)" == "" ]; then
#             echo "Failed to forcefully build zfs-dkms" 1>&2
#             exit 3
#         fi
#     else
#         echo "zfs module is not loaded. Cannot continue." 1>&2
#         exit 3
#     fi
# fi

apt -y install zfsutils-linux

MAX_DISKS=${#DISKS[@]};

echo "Using $MAX_DISKS Disks"

CUR_DISK=0;

BOOT_PARTS=""
ROOT_PARTS=""

echo "Creating Partitions"
for DISK_ID in "${DISKS[@]}"; do
# while [ $CUR_DISK -lt $MAX_DISKS ]; do
    DISK_PATH="/dev/disk/by-id/${DISK_ID}"
    if [ ! -b $DISK_PATH ]; then
        echo "${DISK_PATH} is not a block device" 1>&2
        exit 1
    fi
    HAVE_TOUCHED=$(touched_disk "${DISK_PATH}")
    if [ ${HAVE_TOUCHED} -eq 0 ]; then
        echo "Creating Partitions for $DISK_PATH"
        # uefi
        sgdisk -n2:1M:+512M -t2:EF00 "$DISK_PATH"
        # boot
        sgdisk -n3:0:+1G   -t3:BF00 "$DISK_PATH"
        # root
        sgdisk -n4:0:+${ROOT_SIZE} -t4:BF00 "$DISK_PATH"

        BOOT_PARTS="$BOOT_PARTS $DISK_PATH-part3"
        ROOT_PARTS="$ROOT_PARTS $DISK_PATH-part4"
    fi
    # CUR_DISK=$((CUR_DISK + 1))
done


for DISK_ID in "${DISKS[@]}"; do
    DISK_PATH="/dev/disk/by-id/${DISK_ID}"
    dev=$(lsblk -S ${DISK_PATH} | grep disk | awk '{print $1}')
    echo 1 > /sys/block/${dev}/device/rescan
done
echo "Waiting for disks to settle"
sleep 5
echo "Creating Pools"

BPOOL_ARGS="-o cachefile=/etc/zfs/zpool.cache -o ashift=12 -d -o feature@async_destroy=enabled -o feature@bookmarks=enabled -o feature@embedded_data=enabled -o feature@empty_bpobj=enabled -o feature@enabled_txg=enabled -o feature@extensible_dataset=enabled -o feature@filesystem_limits=enabled -o feature@hole_birth=enabled -o feature@large_blocks=enabled -o feature@lz4_compress=enabled -o feature@spacemap_histogram=enabled -o feature@zpool_checkpoint=enabled -O acltype=posixacl -O canmount=off -O compression=lz4 -O devices=off -O normalization=formD -O relatime=on -O xattr=sa"
RPOOL_ARGS="-o ashift=12 -O acltype=posixacl -O canmount=off -O compression=lz4 -O dnodesize=auto -O normalization=formD -O relatime=on"

# POOL Creation
zpool create ${RPOOL_ARGS} -O mountpoint=/ -R /mnt rpool mirror ${ROOT_PARTS}
zpool create ${BPOOL_ARGS} -O mountpoint=/boot -R /mnt bpool mirror ${BOOT_PARTS}

# Root Pool, OS Location
zfs create -o canmount=noauto -o mountpoint=/ rpool/DEBIAN
zfs mount rpool/DEBIAN

# Boot Pool, Boot directory
zfs create -o mountpoint=/boot -o canmount=on bpool/BOOT

# Root Pool, Extra Directories
zfs create -o mountpoint=/etc rpool/DEBIAN/etc
zfs create -o mountpoint=/var/log rpool/log
zfs create rpool/home
zfs create -o mountpoint=/root rpool/home/root
chmod 700 /mnt/root

mkdir /mnt/run
mount -t tmpfs tmpfs /mnt/run
mkdir /mnt/run/lock

debootstrap ${VERSION_CODENAME} /mnt

mkdir /mnt/etc/zfs
cp /etc/zfs/zpool.cache /mnt/etc/zfs/

echo "${HOSTNAME}" > /mnt/etc/hostname
echo "${HOSTNAME}" > /mnt/etc/mailname

cat <<EOF >/mnt/etc/hosts
127.0.0.1   localhost
127.0.1.1   ${HOSTNAME}

::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOF

cat <<EOF >/mnt/etc/nftables.conf
#!/usr/sbin/nft -f

flush ruleset

table inet filter {
        chain input {
                type filter hook input priority 0; policy accept;
        }
        chain forward {
                type filter hook forward priority 0; policy accept;
        }
        chain output {
                type filter hook output priority 0; policy accept;
        }
}
EOF

cat <<EOF > /mnt/etc/apt/sources.list
deb http://deb.debian.org/debian ${VERSION_CODENAME} main contrib
deb-src http://deb.debian.org/debian ${VERSION_CODENAME} main contrib

deb http://deb.debian.org/debian-security ${VERSION_CODENAME}-security main contrib
deb-src http://deb.debian.org/debian-security ${VERSION_CODENAME}-security main contrib

deb http://deb.debian.org/debian ${VERSION_CODENAME}-updates main contrib
deb-src http://deb.debian.org/debian ${VERSION_CODENAME}-updates main contrib
EOF

for p in proc sys dev; do
    mount --rbind /${p} /mnt/${p}
done

cp zfs-install.part2.sh /mnt/root/
cp zfs-prep.sh /mnt/root/
mkdir -p /mnt/usr/local/sbin/
cp post-update /mnt/usr/local/sbin/
echo "${DISKS[@]}" > /mnt/root/disklist

echo "Part 1 Complete. Please chroot to /mnt and then run /root/zfs-install.part2.sh"
chroot /mnt /bin/bash /root/zfs-install.part2.sh
