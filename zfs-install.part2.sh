#!/usr/bin/env bash
# Copyright 2024-2024 by AccidentallyTheCable <cableninja@cableninja.net>.
# All rights reserved.
# This file is part of AccidentallyTheCables ZFS Installer Scripts,
# and is released under "AGPLv3". Please see the LICENSE
# file that should have been included as part of this package.
#### END COPYRIGHT BLOCK ###

TOUCHED_DISKS=""

touched_disk() {
    local d
    local serial
    d=$1
    serial=$(lsblk -S $d | grep disk | awk '{print $7}')
    if [ "$(echo "${TOUCHED_DISKS}" | grep "${serial}")" != "" ]; then
        #echo "ALREADY TOUCHED ${d} Serial: ${serial}"
        echo 1
    else
        #echo "HAVE NOT TOUCHED ${d} YET Serial: ${serial}"
        TOUCHED_DISKS="${TOUCHED_DISKS} ${serial}"
        echo 0
    fi
}

if [ ! -f /root/disklist ]; then
    echo "Could not find disklist that should have been exported by zfs-install.sh" 1>&2
    exit 1
fi

DISKS=($(cat /root/disklist))

if [ ${#DISKS[@]} -eq 0 ]; then
    echo "No Disks Found" 1>&2
    exit 1
fi

if [ ! -f /etc/mtab ]; then
    ln -s /proc/self/mounts /etc/mtab
fi

export DEBIAN_FRONTEND=noninteractive

apt update
apt install -y locales console-setup
apt install -y dpkg-dev linux-headers-amd64 linux-image-amd64
apt install -y dosfstools
apt install -y zfs-initramfs

echo REMAKE_INITRD=yes >> /etc/dkms/zfs.conf

echo "Creating First EFI Partition"
DISK_0="${DISKS[0]}"
touched_disk $DISK_0 2>/dev/null
mkdosfs -F 32 -s 1 -n EFI /dev/disk/by-id/${DISK_0}-part2
echo "/dev/disk/by-id/${DISK_0}-part2 /boot/efi vfat defaults 0 0" >> /etc/fstab
mkdir /boot/efi
mount /boot/efi

apt -y install grub-efi-amd64 shim-signed
apt -y remove --purge os-prober


cp /usr/share/systemd/tmp.mount /etc/systemd/system
systemctl enable tmp.mount

grub-probe /boot

update-initramfs -c -k all

cat <<EOF >>/etc/default/grub
GRUB_CMDLINE_LINUX="root=ZFS=rpool/DEBIAN"
GRUB_CMDLINE_LINUX_DEFAULT=""
GRUB_TERMINAL=console
EOF

update-grub

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=debian --recheck --no-floppy

echo "Copying EFI Partition to other disks"
SKIP_FIRST=0
for DISK_ID in "${DISKS[@]}"; do
    if [ $SKIP_FIRST -eq 0 ]; then
        SKIP_FIRST=1
        continue
    fi
    DISK_PATH="/dev/disk/by-id/${DISK_ID}"
    if [ ! -b $DISK_PATH ]; then
        echo "${DISK_PATH} is not a block device" 1>&2
        exit 1
    fi
    HAVE_TOUCHED=$(touched_disk "${DISK_PATH}")
    if [ ${HAVE_TOUCHED} -eq 0 ]; then
        echo "Operating on ${DISK_PATH}"
        dd if=/dev/disk/by-id/${DISK_0}-part2 of=${DISK_PATH}-part2
        echo "# ${DISK_PATH}-part2 /boot/efi vfat defaults 0 0 # Reserve EFI" >> /etc/fstab
    fi
done

systemd-firstboot --force --setup-machine-id --hostname=$(cat /etc/hostname) --locale=en_US.UTF-8 --locale-messages=en_US.UTF-8 --timezone=Etc/Universal
hostname -b $(cat /etc/hostname)
apt -y install openssh-server sudo

# Post install fixes
# chown systemd-network:systemd-network -R /run/systemd/netif/state
apt -y install wireless-regdb
GROUPS=(input render sgx kvm netdev)
for g in ${GROUPS[@]}; do
    groupadd -r ${g}
done

echo "Installer Part 2 is complete. Please Exit the chroot and run zfs-cleanup.sh"
echo "!!!!!!!!!" 1>&2
echo "!!!!!!!!!" 1>&2
echo "THE ROOT PASSWORD MUST STILL BE SET, BE SURE TO SET THE ROOT PASSWORD" 1>&2
echo "!!!!!!!!!" 1>&2
echo "!!!!!!!!!" 1>&2
