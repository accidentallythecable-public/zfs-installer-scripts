#!/usr/bin/env bash
# Copyright 2024-2024 by AccidentallyTheCable <cableninja@cableninja.net>.
# All rights reserved.
# This file is part of AccidentallyTheCables ZFS Installer Scripts,
# and is released under "AGPLv3". Please see the LICENSE
# file that should have been included as part of this package.
#### END COPYRIGHT BLOCK ###

echo "Running Snapshot and Cleanup" 

zfs snapshot -r bpool@install-$(date "+%Y%m%d")
zfs snapshot -r rpool@install-$(date "+%Y%m%d")

umount /mnt/boot/efi
for i in proc sys dev run; do
    umount -lf /mnt/$i
done

# Do it 3 times, always 3 times
# Seriously though, do it 3 times because mount ordering
zfs list | grep -v "NAME" | awk '{print $1;}' | while read m; do zfs umount $m; done
zfs list | grep -v "NAME" | awk '{print $1;}' | while read m; do zfs umount $m; done
zfs list | grep -v "NAME" | awk '{print $1;}' | while read m; do zfs umount $m; done

zpool export -a

echo -e "\nINSTALLATION IS NOW COMPLETE. THE SYSTEM CAN BE REBOOTED"
echo "NETWORKING WILL STILL NEED TO BE CONFIGURED ON RETURN"
echo "ALTERNATIVELY, YOU MAY CHROOT AND CONFIGURE NETWORKING BEFORE REBOOT"
