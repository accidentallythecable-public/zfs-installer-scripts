#!/usr/bin/env bash
# Copyright 2024-2024 by AccidentallyTheCable <cableninja@cableninja.net>.
# All rights reserved.
# This file is part of AccidentallyTheCables ZFS Installer Scripts,
# and is released under "AGPLv3". Please see the LICENSE
# file that should have been included as part of this package.
#### END COPYRIGHT BLOCK ###

echo "Starting.."

if [ "$EUID" -ne 0 ]; then
    echo "Must be run as root" 1>&2
    exit 1
fi

print_help() {
    echo -e "ZFS PREP"
    echo -e "Options:"
    echo -e "\t-i <interface>\t\tInterface to configure"
    echo -e "\t-I <IP>\t\tIP Address to set on Interface, Include subnet"
    echo -e "\t-G <IP>\t\tGateway IP address for Interface"
    echo -e "\t-n <IP>\t\tNameservers to use for DNS Resolution, can be used more than once"
    echo -e "\t-S <IP>\t\tSource IP Address to whitelist for SSH Access, can be used more than once"
    echo ""
    exit 2
}

if [ "$1" == "" ]; then
    print_help
fi

while getopts 'hI:G:n:S:i:' opt; do
        case $opt in
                i) IFACE=$OPTARG
                        ;;
                I) IP_ADDR=$OPTARG
                        ;;
                G) IP_GW=$OPTARG
                        ;;
                n) NAMESERVERS+=("$OPTARG")
                        ;;
                S) SOURCE_IPS+=("$OPTARG")
                        ;;
                *) print_help
                        ;;
        esac
done

if [ "$IP_ADDR" == "" ] || [ "$IP_GW" == "" ] || [ ${#NAMESERVERS[@]} -eq 0 ] || [ ${#SOURCE_IPS[@]} -eq 0 ]; then
    echo "Missing one or more arguments"
    print_help
fi

echo "Configuring..."
cat <<EOF >/etc/network/interfaces
iface $IFACE inet static
    address ${IP_ADDR}
    gateway ${IP_GW}
EOF

echo -n "" > /etc/resolv.conf
for n in "${NAMESERVERS[@]}"; do
    echo "nameserver ${n}" >> /etc/resolv.conf
done

echo "Setting up firewall"

cat <<EOF >/etc/nftables.conf
table inet filter {
    chain input {
        type filter hook input priority 0; policy drop;
        ct state {related, established} accept;
EOF

for src in "${SOURCE_IPS[@]}"; do
    echo "       ip saddr ${src} accept;" >> /etc/nftables.conf
done

cat <<EOF >>/etc/nftables.conf
    }
    chain forward {
        type filter hook forward priority 0; policy accept;
    }
    chain output {
        type filter hook output priority 0; policy accept;
    }
}
EOF

nft -f /etc/nftables.conf

echo "Resetting Interface"
ifdown $IFACE
ifup $IFACE

echo "Waiting a few seconds"
sleep 10

echo "Testing... Make sure output below looks ok"

ping -q -c 3 -t 2 4.2.2.1
host google.com
