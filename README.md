# ZFS Installer

Install ZFS On root for Debian based systems.

This contains a small number of scripts to facilitate ZFS on Root installation.

## What you'll need

Needs 5-8 are optional and only used for `zfs-prep.sh`, if your system has dhcp you wont need them. Only needed for static IP configuration.

 1. A Debian Live CD (Latest possible)
 2. A list of /dev/disk/by-id/ IDs for the disks you wish to use. If you dont need only a few disks, you dont need this.
    a. You only need the `wwn-<id>` or `scsi-<id>` values
 3. The Hostname that will be set (Optional), Default: `<hostid>`
 4. The Size of the Root ZFS pool (Optional), Default: `64G`
 5. The Name of the Network Interface that has an active Connection (EX: `eno1`)
 6. An IP Address that can access the internet (For apt)
    1. IP Address, with subnet (EX: 192.168.1.1/32)
    2. Gateway IP
 7. Nameserver IP(s) for DNS resolution during Apt
 8. IP(s) to whitelist for SSH Access during installation.

SSH is not installed as part of `zfs-prep.sh`, you need to install it manually.

### ***NOTE: Secure Boot must be disabled***

***Secure Boot requires a signing key be added to its system database before it can load any modules signed with it. Adding the DKMS signing key to the system requires a reboot, and because we're in a LiveCD doing this process, the DKMS signing key that was added, will not be the same DKMS key that is generated during the next LiveCD load.***

***Secure Boot can be enabled later, but the ZFS modules must be rebuilt and signed in order for things to function. So, probably best to just leave it disabled unless you hate yourself.***

## Running

Be sure to check the output of each script before running the next to verify that things look okay.

 1. At the LiveCD Menu, edit the boot line (Optional, you'll be in a gui otherwise)

    a. Change `splash` to `nosplash` on the `linux` line

    b. Add `text` to the end of the `linux` line

 2. Run the `zfs-prep.sh` script. See `zfs-prep.sh -h` for options

    a. This is optional, you can just configure the network manually. thats all this script does.

 3. Run the `zfs-install.sh` script. See `zfs-install.sh -h` for options

    a. If you dont need specific disks, you can pass the `-A` option, otherwise use `-d` for each disk.

    b. This script will copy `zfs-install.part2.sh` to `/mnt/root/` as part of the process
        - This script also relies on `/mnt/root/disklist` which is the list of disks passed during `zfs-install.sh`

 4. `zfs-install.sh` automatically runs `zfs-install.part2.sh` in chroot.
 5. Once Completed, Enter Chroot via `chroot /mnt`
 6. Configure Networking

    a. You can run `zfs-prep.sh` inside the chroot, or...

    b. Configure the network manually

 7. While still in chroot, Set the root password via `passwd root`
    1. Also should probably create a non-root user
    2. Give that user sudo access
    3. If no non-root user, be sure to enable root password auth in `/mnt/etc/ssh/sshd_config`, unless you copy SSH-Keys to the root user.
 8. (Optional) `dpkg-reconfigure locales tzdata` (While in chroot)
 9.  Exit the chroot'd environment.
 10. Run the `zfs-cleanup.sh` script. There are no arguments for this script.
 11. Reboot

### Example Installer commands

   - `./zfs-installer.sh -H myhostname -A`: Use All Disks, default rpool size, `myhostname` as hostname
   - `./zfs-installer.sh -R 100G -d wwn-0x50afa1459af -d wwn-0x5000a221daf`: Use 2 specific disks, 100GB rpool, default hostname (hostid will be used as hostname)

## Recovery

A Recovery script, `zfs-recovery.sh`, exists to get everything mounted relative to /mnt in preparation for chroot in the event of problems. This script will install required packages, mount pools and everything required for chroot.

Once you're done with recovery, you can run `zfs-cleanup.sh`

## Operation

### `zfs-prep.sh`

 - Configure Installer /etc/network/interfaces
 - Configure Installer /etc/resolv.conf
 - Configure Installer /etc/nftables.conf
 - Put Firewall rules in place on installer
 - Down + Up Network Interface
 - Test Network

### `zfs-install.sh`

 - Add Installer Env Apt Sources
 - Install packages for bootstrapping
 - Install ZFS things
 - For Specified disks, Create Partitions (see below)
 - Create zpool `bpool` (For Boot)
 - Create zpool `rpool` (For OS)
 - Create zfs datasets for
   - /etc
   - /var/log
   - /home
   - /root
 - Execute `debootstrap`
 - Set Hostname
 - Add Apt Sources
 - Prepare for chroot (Mount /dev /proc /sys)
 - Copy scripts into chroot area
   - `zfs-install.part2.sh`
   - `zfs-prep.sh`
 - Create `/mnt/root/disklist` for Part 2

### `zfs-install.part2.sh`

 - Install some required packages
 - Install Kernel
 - Install zfs-initramfs
 - Create EFI at Partition 2 of the first disk in `/mnt/root/disklist`
 - Configure and Install Grub
 - Execute `update-initramfs`
 - `dd` EFI to all other disks
 - Install SSH, Sudo
 - Extra post-install tasks
   - Ensures some packages that might be required are installed
   - Ensures some required groups exist

### `zfs-cleanup.sh`

 - Create Snapshots of `bpool` and `rpool` recursively
 - Unmount /dev /proc /sys
 - Unmount all ZFS mounts
 - ZFS Export

### `zfs-recovery.sh`

 - Add Installer Env Apt Sources
 - Install ZFS things
 - Mount Pools relative to /mnt
 - Prepare for chroot (Mount /dev /proc /sys)

### `post-update`

This script should be run any time any operation manipulates kernel/grub/efi, etc.

 - Configure and Install Grub
 - Execute `update-initramfs`
 - `dd` EFI to all other disks
